const mongoose = require('mongoose');
const scheduleSchema = mongoose.Schema({
        immediate_cost: { type: Number, required: false},
        service_name: { type: String, required: false },
        amount: { type: Number, required: false },
        frequency: { type: Number, required: false },
        note: { type: String, required: false },
        schedule_id: { type: String, required: false },
        schedule_type: { type: Number, required: false },
        payment_day: { type: Number, required: false },
        payment_month: { type: Number, required: false },
        qr_code: { type: String, required: false },
        status: { type: String, required: false },
        created_at : { type: Date, required: true, default: Date.now },
        creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: false }
        
   
});
module.exports = mongoose.model('static-invoice-schedules', scheduleSchema);