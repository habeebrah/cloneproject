import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PostListComponent } from "./posts/post-list/post-list.component";
import { PostCreateComponent } from "./posts/post-create/post-create.component";
import { HomeComponent } from "./home/home.component";
import { HomeViewComponent } from "./home/home-view/home-view.component";
import { HomeListComponent } from "./home/home-list/home-list.component";
import { ProductComponent } from "./product/product.component";
import { ProductAComponent } from "./product/product-productA/product-productA.component";
import { ProductBComponent } from "./product/product-productB/product-productB.component";
import { ProductCComponent } from "./product/product-productC/product-productC.component";
import { InvoiceComponent } from "./invoice/invoice.component";
import { InvoiceSuccessComponent } from "./invoice/invoice-success/invoice-success.component";

const routes: Routes = [
    { path: '', component: HomeListComponent},
    { path: 'post', component: PostListComponent },
    { path: 'create', component: PostCreateComponent },
    { path: 'edit/:postId', component: PostCreateComponent },
    { path: 'product', component: ProductComponent },
    { path: 'product/:id', component: HomeViewComponent },
    { path: 'productA', component: ProductAComponent },
    { path: 'productB', component: ProductBComponent },
    { path: 'productC', component: ProductCComponent },
    { path: 'invoice', component: InvoiceComponent},
    { path: 'invoice/:id', component: InvoiceComponent},
    { path: 'invoice/:status', component: InvoiceComponent},
    { path: 'success', component: InvoiceSuccessComponent},
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}