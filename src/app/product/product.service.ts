import { Product } from './product.model';
import { Invoice } from '../invoice/invoice.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Constants } from '../constants';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';
import { nextTick } from 'q';


@Injectable({
	providedIn:'root'
})

export class ProductService {
    private product: Product[] = [];
    private invoice: Invoice[] = [];
    invoiceId:string;
    invoiceType:string = null
    private postsUpdated = new Subject<{product: Product[], postCount: number}>();
	//uri = 'http://localhost:3000/api/';
    //uri = 'http://51.158.68.20:5000/api/';
    uri : string;


    constructor(private http: HttpClient, private router: Router) {
        this.uri = Constants.API_ENDPOINT;
    }

        
        addShedule(immediate_cost:number,
            service_name: string,
            amount: number,
            frequency: number,
            payment_day: number,
            payment_month: number,
            payment_date :string,
            schedule_type:number,
            invoiceType: string
                ){
                
        const product:Product = {
                                immediate_cost: immediate_cost,
                                service_name: service_name,
                                amount: amount,
                                frequency: frequency,
                                payment_day: payment_day,
                                payment_month: payment_month,
                                schedule_type: schedule_type,                                
                            };
       
                           
        this.http
        .post<{shedulePadentId: string, sheduleId: string, qr_code: string}>(this.uri + 'shedule/', product)
        .subscribe( responseData => {
            console.log(responseData.shedulePadentId);
            const invoice:Invoice = {
                paymentDay:payment_day,
                immediatePaymentAmount:immediate_cost,
                paymentAmount:amount,
                invoiceType:invoiceType,
                frequency: frequency,
                payment_month: payment_month,
                payment_date: payment_date,
                schedule_type: schedule_type,
                qr_code: responseData.qr_code,
                shedule_parent_id:responseData.shedulePadentId,
                shedule_id:responseData.sheduleId
            };

            this.http
            .post<{id: string,invoice_parent_id: string}>(this.uri + 'invoice/', invoice)
            .subscribe( responseInvoiceData => {
                //console.log(responseInvoiceData.invoiceId);
               
               
                this.router.navigate(["/invoice", responseInvoiceData.id]);
            });
            },
            err => {
                return err;
            });
        
    }
	

}