const mongoose = require('mongoose');
const invoiceSchema = mongoose.Schema({

   
    //schedule: [{ type: mongoose.Schema.Types.String, ref:'Schedule'}],
    //schedule:String,
  
    //paymentdate: Date,
    //QRcode: String,
    //status: Number,
    //comments: String
    _id: mongoose.Schema.Types.ObjectId,
    number: { type: Number, required: false },
    immediate_cost: { type: Number, required: false },
    regular_amount: { type: Number, required: false },
    payment_amount: { type: Number, required: false },
    frequency: { type: Number, required: false },
    invoice_id: { type: String, required: false },
    schedule_id: { type: String, required: false },
    schedule_parent_id: { type: mongoose.Schema.Types.ObjectId, ref: 'static-invoice-schedules' },
    schedule_type: { type: Number, required: false },
    payment_day: { type: Number, required: false },
    payment_month: { type: String, required: false },
    payment_date : { type: Date, required: false },
    payment_status: { type: Number, required: false },
    paid_on: { type: String, required: false },
    qr_code: { type: String, required: false }
    
    //creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});
module.exports = mongoose.model('Invoices', invoiceSchema);